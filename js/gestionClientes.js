var clientesObtenidos;

function getCustomers() {
  var url= "https://services.odata.org/V4/Northwind/Northwind.svc/Customers";
  var request= new XMLHttpRequest();
  request.onreadystatechange = function() {
    if(this.readyState == 4 && this.status == 200){
      console.log(request.responseText);
      clientesObtenidos = request.responseText;
      procesarClientes();
    }
  }
  request.open("GET", url, true);
  request.send();

  }

  function procesarClientes() {
    var JSONClientes = JSON.parse(clientesObtenidos);
    var rutaBandera = "https://www.countries-ofthe-world.com/flags-normal/flag-of-";
    var divTabla = document.getElementById('divTablaClientes');
    var tabla = document.createElement("table");
    var tbody = document.createElement("tbody");
    tabla.classList.add("table");
    tabla.classList.add("table-striped");
    //alert(JSONProductos.value[0].ProductName);
    var headers = document.createElement("tr");
    var headerNombre = document.createElement("th");
    headerNombre.innerText = "Nombre";
    var headerPrecio = document.createElement("th");
    headerPrecio.innerText = "Ciudad";
    var headerStock = document.createElement("th");
    headerStock.innerText = "Bandera País";
    headers.appendChild(headerNombre);
    headers.appendChild(headerPrecio);
    headers.appendChild(headerStock);
    tbody.appendChild(headers);
    for (var i = 0; i < JSONClientes.value.length; i++) {
      //console.log(JSONProductos.value[i].ProductName);
      var nuevaFila = document.createElement("tr");

      var columnaNombre = document.createElement("td");
      columnaNombre.innerText = JSONClientes.value[i].ContactName;

      var columnaCiudad = document.createElement("td");
      columnaCiudad.innerText = JSONClientes.value[i].City;

      var imgBandera = document.createElement("img");
      imgBandera.classList.add("flag");
      if (JSONClientes.value[i].Country ==  "UK"){
        imgBandera.src= rutaBandera+"United-Kingdom.png";
      }
      else{
        imgBandera.src= rutaBandera+JSONClientes.value[i].Country+".png";
      }

      nuevaFila.appendChild(columnaNombre);
      nuevaFila.appendChild(columnaCiudad);
      nuevaFila.appendChild(imgBandera);

      tbody.appendChild(nuevaFila);

    }
    tabla.appendChild(tbody);
    divTabla.appendChild(tabla);

}
